# Racket-QApplets


## About

Racket interface to Gentoo's `portage-utils` package.

Portage-Utils upstream: https://github.com/gentoo/portage-utils


## License

Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v3 License

SPDX-License-Identifier: GPL-3.0-only
